package com.epam.task1.bean;

import com.epam.task1.Main;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BeanA implements BeanValidator, InitializingBean, DisposableBean {
    private String name;
    private String value;

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean validate() {
        return !name.isEmpty() && value.length() >= 0;
    }

    @Override
    public void destroy() throws Exception {
        Main.logger.info("BeanA initialized.");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Main.logger.info("BeanA Properties Set.");
    }
}
