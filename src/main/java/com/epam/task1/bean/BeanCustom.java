package com.epam.task1.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.support.GenericBeanDefinition;

public class BeanCustom implements BeanFactoryPostProcessor {
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
        DefaultListableBeanFactory factory = (DefaultListableBeanFactory) configurableListableBeanFactory;
        GenericBeanDefinition definition = new GenericBeanDefinition();
        definition.setBeanClass(BeanB.class);
        definition.setInitMethodName("initCustom");
        factory.registerBeanDefinition("beanB", definition);
    }
}
