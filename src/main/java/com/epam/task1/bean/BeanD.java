package com.epam.task1.bean;

import com.epam.task1.Main;

public class BeanD implements BeanValidator {
    private String name;
    private String value;

    @Override
    public String toString() {
        return "BeanD{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    @Override
    public boolean validate() {
        return name != null && value.length() >= 0;
    }

    private void init() {
        Main.logger.info("BeanD initialized.");
    }

    private void destroy() {
        Main.logger.info("BeanD destroyed.");
    }
}
