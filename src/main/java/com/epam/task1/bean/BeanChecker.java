package com.epam.task1.bean;

import com.epam.task1.Main;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

@Component
public class BeanChecker implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof BeanValidator) {
            BeanValidator beanValidator = (BeanValidator) bean;
            if (beanValidator.validate()) {
                Main.logger.info(beanName + "  valid");
            }
        }
        return bean;
    }
}
