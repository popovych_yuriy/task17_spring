package com.epam.task1.bean;

public interface BeanValidator {
    boolean validate();
}
