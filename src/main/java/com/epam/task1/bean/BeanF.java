package com.epam.task1.bean;

public class BeanF implements BeanValidator {
    private String name;
    private String value;

    @Override
    public String toString() {
        return "BeanF{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    @Override
    public boolean validate() {
        return !name.isEmpty() && value.length() >= 0;
    }
}
