package com.epam.task1.bean;

import com.epam.task1.Main;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanE implements BeanValidator {
    private String name;
    private String value;

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    @Override
    public boolean validate() {
        return !name.isEmpty() && value.length() >= 0;
    }

    @PostConstruct
    public void postConstruct() {
        Main.logger.info("BeanE post construct");
    }

    @PreDestroy
    public void preDestroy() {
        Main.logger.info("BeanE pre destroy");
    }
}
