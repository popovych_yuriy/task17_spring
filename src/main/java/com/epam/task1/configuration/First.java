package com.epam.task1.configuration;

import com.epam.task1.bean.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class First {
    @Bean(name = "beanD", initMethod = "init", destroyMethod = "destroy")
    public BeanD beanD(){
        return new BeanD();
    }

    @Bean(name = "beanB", initMethod = "init", destroyMethod = "destroy")
    @DependsOn("beanD")
    public BeanB beanB(){
        return new BeanB();
    }

    @Bean(name = "beanC", initMethod = "init", destroyMethod = "destroy")
    @DependsOn("beanB")
    public BeanC beanC(){
        return new BeanC();
    }


    @Bean(name = "firstBeanA")
    public BeanA firstBeanA(){
        BeanA beanA = new BeanA();
        beanA.setName("firstBeanA");
        beanA.setValue(beanB().getValue() + beanC().getValue());
        return beanA;
    }

    @Bean(name = "secondBeanA")
    public BeanA secondBeanA(){
        BeanA beanA = new BeanA();
        beanA.setName("secondBeanA");
        beanA.setValue(beanB().getValue() + beanD().getValue());
        return beanA;
    }

    @Bean(name = "thirdBeanA")
    public BeanA thirdBeanA(){
        BeanA beanA = new BeanA();
        beanA.setName("thirdBeanA");
        beanA.setValue(beanC().getValue() + beanD().getValue());
        return beanA;
    }

    @Bean(name = "firstBeanE")
    public BeanE firstBeanE(@Qualifier(value = "firstBeanA") BeanA beanA){
        BeanE beanE = new BeanE();
        beanE.setName("firstBeanE");
        beanE.setValue(beanA.getValue());
        return beanE;
    }

    @Bean(name = "secondBeanE")
    public BeanE secondBeanE(@Qualifier(value = "secondBeanA") BeanA beanA){
        BeanE beanE = new BeanE();
        beanE.setName("secondBeanE");
        beanE.setValue(beanA.getValue());
        return beanE;
    }

    @Bean(name = "thirdBeanE")
    public BeanE beanE(@Qualifier(value = "thirdBeanA") BeanA beanA){
        BeanE beanE = new BeanE();
        beanE.setName("thirdBeanE");
        beanE.setValue(beanA.getValue());
        return beanE;
    }
}
