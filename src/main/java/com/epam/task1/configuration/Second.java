package com.epam.task1.configuration;

import com.epam.task1.bean.BeanChecker;
import com.epam.task1.bean.BeanCustom;
import com.epam.task1.bean.BeanF;
import org.springframework.context.annotation.*;

@Configuration
@Import(First.class)
public class Second {
    @Bean(name = "beanF")
    @Lazy
    public BeanF beanF() {
        return new BeanF();
    }

    @Bean
    public BeanCustom beanCustom() {
        return new BeanCustom();
    }

    @Bean
    public BeanChecker beanChecker() {
        return new BeanChecker();
    }
}
