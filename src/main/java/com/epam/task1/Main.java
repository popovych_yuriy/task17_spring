package com.epam.task1;

import com.epam.task1.configuration.Second;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                Second.class);
        for (String name : context.getBeanDefinitionNames()) {
            logger.info(name);

            BeanDefinition bean = context.getBeanDefinition(name);
            logger.info(bean);
        }
        context.close();
    }
}
